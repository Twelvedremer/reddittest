//
//  Post.swift
//  RedditTest
//
//  Created by Momentum Lab 1 on 2/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import RealmSwift
import AlamofireObjectMapper
import ObjectMapper

class Post: Object, Mappable{
    
    dynamic var author:String!
    dynamic var title:String!
    dynamic var thumbnail:String?
    dynamic var num_comments:Int = 0
    dynamic var subredditName:String!
    dynamic var created:Date!
   
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        author <- map["data.author"]
        title <- map["data.title"]
        thumbnail <- map["data.thumbnail"]
        num_comments <- map["data.num_comments"]
        subredditName <- map["data.subreddit"]
        created <- (map["data.created"], DateTransform())
        
    }
}
