//
//  PostModelsControllers.swift
//  RedditTest
//
//  Created by Momentum Lab 1 on 2/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import RealmSwift
import AlamofireObjectMapper
import Alamofire

protocol PostViewModelProtocol {
    func updateTable()
}

class PostViewModels{
    
    let URL = "https://www.reddit.com/top/.json"
    var postList:[Post] = []
    var delegate: PostViewModelProtocol!
    
    public func getPostCount() -> Int {
        return postList.count
    }
    
    public func getPostData(_ row:Int) -> Post {
        return postList[row]
    }
    
    init(_ delegate:PostViewModelProtocol) {
        self.delegate = delegate
        loadBd()
    }
    
    public func loadBd(){
        Alamofire.request(URL).responseArray(keyPath: "data.children"){ (response: DataResponse<[Post]>) in
            let postArrays = response.result.value
            let realm = try! Realm()
            if let postArrays = postArrays {
                // hay conexion a internet
                if postArrays.count > 0{
                    try! realm.write {
                        realm.deleteAll()
                        for post in postArrays {
                            self.postList.append(post)
                            realm.add(post)
                        }
                    }
                } else{
                    let post: Results<Post> = { realm.objects(Post.self) }()
                    if post.count > 0 {
                        for  PostItems in post {
                            self.postList.append(PostItems) //se castea al modelo y guarda en la cache
                        }
                    }
                }
            } else{
                
                // no hay conexion a internet, se extrae la informacion local
                let post: Results<Post> = { realm.objects(Post.self) }()
                if post.count > 0 {
                    for  PostItems in post {
                        self.postList.append(PostItems) //se castea al modelo y guarda en la cache
                    }
                }
                
            }
            self.delegate.updateTable()
        }
    }
    
    
}
