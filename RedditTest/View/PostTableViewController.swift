//
//  MainTableViewController.swift
//  RedditTest
//
//  Created by Momentum Lab 1 on 2/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit


class PostTableViewController: UITableViewController {

    
    var postViewModels : PostViewModels!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomTableViewCell")
        loading.startAnimating()
        postViewModels = PostViewModels(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postViewModels.getPostCount()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell") as! CustomTableViewCell
        
        cell.loadLabel(post: postViewModels.getPostData(indexPath.row))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        viewController.row = indexPath.row
        viewController.modelView = self.postViewModels
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    

//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.destination == "detailsView" {
//            
//            let vc = segue.destination as! DetailsViewController
//            vc.colorString = colorLabel.text!
//        }
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
// 

}

extension PostTableViewController:PostViewModelProtocol{
    func updateTable(){
        loading.stopAnimating()
        tableView.reloadData()
    }
}
