//
//  ViewController.swift
//  RedditTest
//
//  Created by Momentum Lab 1 on 2/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var row:Int!
    var modelView:PostViewModels!
        
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var autorLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var subreditLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDetails()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadDetails(){
        
        let post = self.modelView.getPostData(row)
        if let title = post.title, let author = post.author, let created = post.created, let subredit = post.subredditName{
                self.titleLabel.text = title
           
            self.autorLabel.text = "by \(author)"
            self.commentLabel.text = "\(post.num_comments) comments"
            let units: Set<Calendar.Component> = [.hour, .day, .month, .year]
            let comps = Calendar.current.dateComponents(units, from: created)
            if let day = comps.day, let month = comps.month , let year = comps.year {
                self.createdLabel.text = "\(day)/\(month)/\(year)"
            }
            self.subreditLabel.text = "subreddit: \\\(subredit)"
            
        }
        if let thumbnail = post.thumbnail {
            let urlImage = URL(string: thumbnail)!
            let session = URLSession(configuration: .default)
            let downloadPicTask = session.dataTask(with: urlImage) { (data, response, error) in
                
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    if (response as? HTTPURLResponse) != nil {
                        if let imageData = data {
                            self.image.image = UIImage(data: imageData)
                            
                        }
                    }
                }
            }
            downloadPicTask.resume()
        }
        
    }


}

