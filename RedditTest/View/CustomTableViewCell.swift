//
//  CustomTableViewCell.swift
//  RedditTest
//
//  Created by Momentum Lab 1 on 2/15/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadLabel(post:Post){
        
        if let title = post.title, let author = post.author, let created = post.created {
            self.title.text = title
            self.author.text = author
            self.comment.text = "\(post.num_comments) comments"
            let units: Set<Calendar.Component> = [.hour, .day, .month, .year]
            let comps = Calendar.current.dateComponents(units, from: created)
            if let day = comps.day, let month = comps.month , let year = comps.year {
                self.date.text = "\(day)/\(month)/\(year)"
            }
            
        }
        if let thumbnail = post.thumbnail {
            let urlImage = URL(string: thumbnail)!
            let session = URLSession(configuration: .default)
            let downloadPicTask = session.dataTask(with: urlImage) { (data, response, error) in
                
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    if (response as? HTTPURLResponse) != nil {
                        if let imageData = data {
                            self.imageview.image = UIImage(data: imageData)
                            
                        }
                    }
                }
            }
            downloadPicTask.resume()
        }
    }
}

